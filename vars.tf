variable "virtual_machine_name" {}

variable "resource_group_name" {}

variable "request_access_minutes" {
  default = "5"
}

variable "allowed_source_address_prefixes" {
  type = "list"
}
