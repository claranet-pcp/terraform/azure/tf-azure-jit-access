resource "azurerm_template_deployment" "jit_access" {
  name                = "jit-access-${var.virtual_machine_name}"
  resource_group_name = "${var.resource_group_name}"

  template_body = "${file("${path.module}/templates/jit.json")}"

  parameters {
      "vmName" = "${var.virtual_machine_name}"
      "maxRequestAccessDuration" = "PT${var.request_access_minutes}M"
      "allowedSourceAddressPrefixes" = "${join(",",var.allowed_source_address_prefixes)}"
  }

  deployment_mode = "Incremental"
}
